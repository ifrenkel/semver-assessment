require 'thor'
require './lib/vmatcher'

def _cache_dir(options)
  File.join(File.expand_path(File.dirname(__FILE__)), '..', options[:cache_dir])
end

class VersionMatcherShell < Thor
  class_option :advisory_db_path, default: '/tmp/gemnasium-db/'
  class_option :cache_dir, default: '.cache'
  class_option :verbosity, default: :debug
  class_option :ignore_cache_miss, default: false

  desc "managers [MANAGERS]", "check semver and vrange for the list of managers supplied"
  def managers(*managers)
    VersionMatcher::Main.new(managers, options[:advisory_db_path], _cache_dir(options), options[:verbosity], options[:ignore_cache_miss])
      .verify_managers
  end

  desc "package [MANAGER] [PACKAGE]", "check a specified package against semver_dialects and vrange"
  def package(manager, package)
    VersionMatcher::Main.new([manager], options[:advisory_db_path], _cache_dir(options), options[:verbosity], options[:ignore_cache_miss])
      .verify_package(package)
  end
end

VersionMatcherShell.start(ARGV)
