require 'logging'
require 'json'

require 'byebug'
Dir[File.dirname(__FILE__) + '/vmatcher/**/*.rb'].each {|file| require file }

module VersionMatcher
  class Config
    def self.logger
      @logger
    end

    def self.init_logger(verbosity)
      @logger = Logging.logger(STDOUT)
      @logger.level = verbosity
      @logger
    end
  end

  class Main
    def initialize(managers, advisory_db_path, cache_dir, verbosity=:debug, ignore_cache_miss=false)
      @managers = managers
      @cache_dir = cache_dir
      @cache_manager = CacheManager.new(cache_dir, ignore_cache_miss)
      @command_manager = CommandManager.new(@cache_manager)
      @advisory_db = AdvisoryDB.init(managers, advisory_db_path)
      @semver = Semver.new(@advisory_db)
      @vrange = Vrange.init(@cache_manager)

      Config.init_logger(verbosity)
    end

    def verify_managers
      @managers.each do |manager|
        report = MatchReport.new(manager)

        @advisory_db.packages_for(manager).each do |package|
          package_analyzer.analyze(manager, package, report)
        end

        present(report, "vmatch-pkg-mgr-#{manager}")
      end
      cleanup()
    end

    # match_one tests all known versions for the given package against
    # vrange and semver
    def verify_package(package)
      manager = @managers.first
      report = MatchReport.new(manager)

      package_analyzer.analyze(manager, package, report)

      cleanup()

      present(report, "vmatch-pkg-#{package}-for-#{manager}")
    end

    private

    def present(report, title)
      out = File.open(title.gsub('/','.'), 'w')
      StatsPresenter.new(report, out).print_results
      ErrorPresenter.new(report, out).print_results
      DetailedPresenter.new(report, out).print_results
    end

    def match_package(manager, package, report)
      raise NotImplementedError
    end

    def package_analyzer
      @package_analyzer ||= PackageAnalyzer.new(@command_manager, @cache_manager, @semver, @vrange)
    end
    
    def cleanup
      @cache_manager.flush_caches()
    end
  end
end
