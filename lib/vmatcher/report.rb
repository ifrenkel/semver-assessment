module VersionMatcher
  class Record
    attr_accessor :pkg, :ver, :adv, :rng

    def initialize(pkg, ver, adv, rng)
      @pkg = pkg
      @ver = ver
      @adv = adv
      @rng = rng
    end

    def hash
      [self.class, @pkg, @ver, @adv, @rng].hash
    end

    def eql?(o)
      key == o.key
    end

    def <=>(o)
      key <=> o.key
    end

    def key
      "#{@pkg}:#{@ver} - #{@rng} - #{@adv}"
    end
  end

  class EmptyRecord < Record
    def initialize
      @pkg = ""
      @ver = ""
      @adv = ""
      @rng = ""
    end
  end

  class ErrorRecord < Record; end

  class MatchReport
    attr_accessor :manager, :vrange, :semver, :package_manager_errors, :package_versions, :vrange_errors, :semver_errors

    def initialize(manager)
      @manager = manager
      @vrange = Array.new
      @semver = Array.new
      @package_manager_errors = {}
      @package_versions = {}
      @vrange_errors = {}
      @semver_errors = {}
    end

    def add_package_manager_error(package, err=nil)
      @package_manager_errors[package] = err
    end

    def add_package_versions(package, versions)
      @package_versions[package] = versions
    end

    def add_vrange(matches)
      @vrange += matches
    end

    def add_vrange_error(package, versions, err)
      @vrange_errors[package] = {versions: versions, error: err}
    end

    def add_semver(matches)
      @semver += matches
    end

    def add_semver_error(package, versions, err)
      @semver_errors[package] = {versions: versions, error: err}
    end

    def only_in_vrange
      (@vrange-@semver)
    end

    def only_in_semver
      (@semver-@vrange)
    end

    def print_results
      print_errors
      print_mismatches
      print_stats
    end

    def print_mismatches
      write("vrange mismatches:")
      only_in_vrange = @vrange - @semver
      group_records(only_in_vrange) do |pkg, versions, range, id|
        write(" #{pkg} not matches for #{range} (id: #{id})")
        write(" on versions #{versions.join(',')}")
      end

      write("semver mismatches:")
      only_in_semver = @semver - @vrange
      group_records(only_in_semver) do |pkg, ver, range, id|
        write("#{pkg}:#{ver} not matched in #{range} (id: #{id})")
      end
    end

    def group_records(records, &block)
      result = {}
      records.each do |record|
        key = "#{record.pkg} - #{range}"
        result[key] ||= {versions:[], record: record}
        result[key][:versions] << record.ver
      end

      result.each do |key, val|
        rec = val[:record]
        versions = val[:versions]
        yield(rec.pkg, versions, rec.range, rec.id)
      end
    end

    def inspect
      "vrange: #{@vrange.count}, semver: #{semver.count}"
    end
  end
end
