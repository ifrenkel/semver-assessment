module VersionMatcher
  class Config
    def self.logger
      return @logger unless @logger.nil?

      @logger = Logging.logger(STDOUT)
      @logger.level = @verbosity
      @logger
    end
  end
end
