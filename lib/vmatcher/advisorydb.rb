module VersionMatcher
  class AdvisoryDBPacakgeNotFoundError < StandardError; end

  class AdvisoryDB
    attr_accessor :data
    def initialize(basepath)
      @basepath = basepath
      @data = {}
    end

    def self.init(pkgmgrs, path)
      advdb = self.new(path)
      pkgmgrs.each do |mgr|
        Dir["#{path}/#{mgr}/**/*"].each do |entry|
          advdb.add_path(mgr, entry)
        end
      end
      advdb
    end

    def add_path(pkgmgr, path)
      return unless path.end_with?('.yml')
      @data[pkgmgr] ||= {}
      pkgmgr_full_path = Pathname.new(@basepath) + pkgmgr
      pkg_full_path = Pathname.new(path).dirname
      pkg = pkg_full_path.relative_path_from(pkgmgr_full_path).to_s
      @data[pkgmgr][pkg] ||= []
      @data[pkgmgr][pkg] << path
    end

    def packages_for(manager)
      return [] unless @data.has_key?(manager)
      @data[manager].keys
    end

    def pkgs(mgr)
      return [] unless @data.has_key?(mgr)
      @data[mgr].keys
    end

    def advs(manager, package)
      advisories = @data.dig(manager, package)
      raise AdvisoryDBPacakgeNotFoundError, "#{manager}/#{package}" if advisories.nil?
      advisories.map { |path| adv(path) }
    end

    def adv(path)
      f = File.read(path)
      adv = YAML.load(f)
      res = { "affected_range" => adv["affected_range"], "identifier" => adv["identifier"] }
    end

    def inspect
      @basepath
    end
  end
end
