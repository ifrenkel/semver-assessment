module VersionMatcher
  class MatcherError < StandardError
    attr_accessor :package_manager, :package, :version, :error
    def initialize(package_manager, package, version, err)
      @package_manager = package_manager
      @package = package
      @version = version
      msg = "#{self.class}: #{package_manager}/#{package}:#{version} (err=#{err})"
      super(msg)
    end
  end
end
