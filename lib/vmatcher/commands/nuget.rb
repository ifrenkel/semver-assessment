require 'uri'
require 'net/http'
require 'nokogiri'

module VersionMatcher
  class Nuget < Command
    def fetch(pkg)
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { fetch_from_nuget(pkg) })
      format(output)
    end

    private

    def get_cmd(pkg)
      "get https://www.nuget.org/packages/#{pkg}/atom.xml"
    end

    def create_uri(pkg)
      URI("https://www.nuget.org/packages/#{pkg}/atom.xml")
    end

    def fetch_from_nuget(pkg)
      @rate_controller.pause()

      uri = create_uri(pkg)
      resp = Net::HTTP.get_response(uri)
      unless resp.is_a?(Net::HTTPSuccess)
        raise StandardError, "nuget: error when fetching #{uri} - #{resp.code}"
      end
      resp.body
    end

    def format(xml_str)
      xml = Nokogiri::XML(xml_str)
      xml.remove_namespaces!
      xml.xpath('//entry/id').map { |id| id.text.split('/')[-1] }
    end
  end
end
