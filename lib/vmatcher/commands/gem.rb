module VersionMatcher
  class GemLister < Command
    private

    def get_cmd(pkg)
      # todo: versions is deprecated, need another command
      "docker run --rm ruby:3 gem list -r -a -e '#{pkg}'"
    end

    def format(str)
      match = /[\(]+([^\)]+)/.match(str)
      if match.nil? || match.length != 2
        raise StandardError, "error parsing gem list output - #{str}"
      end
      match[1].scan(/([^, $]+)/).flatten
    end
  end
end
