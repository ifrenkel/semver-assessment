module VersionMatcher
  class Pypi < Command

    private

    def get_cmd(pkg)
      # todo: versions is deprecated, need another command
      "docker run --rm python:3 pip index versions #{pkg}"
    end

    def format(str)
      matches = /Available versions: ([^\n]+)\n/.match(str)
      if matches.nil? || matches.length != 2
        return []
      end
      matches[1].scan(/([^, $]+)/).flatten
    end
  end
end
