require 'uri'
require 'net/http'
require 'nokogiri'

module VersionMatcher
  class Maven < Command
    def fetch(pkg)
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { fetch_from_maven(pkg) })
      format(output)
    end

    private

    def get_cmd(pkg)
      "get https://repo1.maven.org/maven2 #{pkg}"
    end

    def create_uri(prefix, pkg)
      prefix_url = prefix.gsub('.', '/')
      mvnrepo_url = URI("https://repo1.maven.org/maven2/#{prefix_url}/#{pkg}/maven-metadata.xml")
    end

    def fetch_from_maven(fullpkg)
      @rate_controller.pause()

      prefix, pkg = fullpkg.split('/')

      raise StandardError, "invalid fully qualified package name: #{fullpkg}" if pkg.nil?

      uri = create_uri(prefix, pkg)
      resp = Net::HTTP.get_response(uri)
      unless resp.is_a?(Net::HTTPSuccess)
        raise StandardError, "maven: error when fetching #{uri} - #{resp.code}"
      end
      resp.body
    end

    def format(xml_str)
      xml = Nokogiri::XML(xml_str)
      xml.xpath('/metadata/versioning/versions/version').map { |x| x.text }
    end
  end
end
