require 'open3'

module VersionMatcher
  class CommandManager
    def initialize(cache_manager)
      @cache_manager = cache_manager
    end

    def command_for(package_manager_name, rc_backoff=0.25)
      @managers ||= {}

      unless @managers.has_key?(package_manager_name)
        klass = command_class(package_manager_name)

        rc = RequestRateController.new(rc_backoff)
        c = @cache_manager.cache_for(package_manager_name)

        @managers[package_manager_name] = klass.new(c, rc)
      end

      @managers[package_manager_name]
    end

    private

    def command_class(name)
      case name
      when 'pypi'
        Pypi
      when 'maven'
        Maven
      when 'gem'
        GemLister
      when 'nuget'
        Nuget
      when 'packagist'
        Packagist
      when 'npm'
        Npm
      when 'go'
        Go
      when 'vrange'
        Vrange
      else
        raise "command controller for package manager #{name} not found"
      end
    end
  end
end
