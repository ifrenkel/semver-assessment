require 'uri'
require 'net/http'

module VersionMatcher
  class Npm < Command
    def fetch(pkg)
      cmd = get_cmd(pkg)
      output = @cache.fetch(cmd, -> { fetch_from_packagist(pkg) })
      format(output)
    end

    private

    def get_cmd(pkg)
      "get https://registry.npmjs.org/#{pkg}"
    end

    def create_uri(pkg)
      URI("https://registry.npmjs.org/#{pkg}")
    end

    def fetch_from_packagist(pkg)
      @rate_controller.pause()

      uri = create_uri(pkg)
      resp = Net::HTTP.get_response(uri)
      unless resp.is_a?(Net::HTTPSuccess)
        raise StandardError, "npm: error when fetching #{uri} - #{resp.code}"
      end
      resp.body
    end

    def format(str)
      obj = JSON.parse(str)
      versions = obj.dig('versions')
      if versions.nil?
        puts "error getting versions from #{obj}"
        return []
      end
      versions.keys
    end
  end
end
