require 'rspec'
require './lib/vmatcher'

RSpec.describe VersionMatcher::MismatchReportPresenter do
  describe '#matched_versions_diff' do
    context 'both reports have and range' do
      let(:report) do
        r = VersionMatcher::MatchReport.new('a_manager')
        v = [VersionMatcher::Record.new('pkg1', 'v1', 'adv1', 'rng1'), VersionMatcher::Record.new('pkg1', 'v2', 'adv1', 'rng1')]
        r.add_vrange(v)
        v = [VersionMatcher::Record.new('pkg1', 'v1', 'adv1', 'rng1'), VersionMatcher::Record.new('pkg1', 'v3', 'adv1', 'rng1')]
        r.add_semver(v)
        r
      end

      it 'shows disjoint ranges' do
        expected = { 'vrange' => { 'pkg1' => { 'rng1' => ['v3'] } }, 
                     'semver' => { 'pkg1' => { 'rng1' => ['v2'] } }}
        actual = described_class.new(report).matched_versions_diff
        expect(expected).to eql(actual)
      end
    end

    context 'disjoint range' do
      let(:report) do
        r = VersionMatcher::MatchReport.new('a_manager')
        v = [VersionMatcher::Record.new('pkg1', 'vrange-v1', 'adv1', 'vrange-rng-1'), VersionMatcher::Record.new('pkg1', 'vrange-v2', 'adv1', 'vrange-rng-1')]
        r.add_vrange(v)
        v = [VersionMatcher::Record.new('pkg1', 'semver-v1', 'adv1', 'semver-rng-1'), VersionMatcher::Record.new('pkg1', 'semver-v2', 'adv1', 'semver-rng-1')]
        r.add_semver(v)
        r
      end

      it 'shows disjoint ranges' do
        expected = { 'vrange' => { 'pkg1' => { 'semver-rng-1' => ['semver-v1', 'semver-v2'], 'vrange-rng-1' => [] } }, 
                     'semver' => { 'pkg1' => { 'vrange-rng-1' => ['vrange-v1', 'vrange-v2'], 'semver-rng-1' => [] } }}
        actual = described_class.new(report).matched_versions_diff
        expect(actual).to eql(expected)
      end
    end
  end
end
